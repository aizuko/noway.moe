export const load = async ({ fetch }) => {
    const response = await fetch("/api/duckietown")
    const notes = await response.json()
    return { notes }
}
