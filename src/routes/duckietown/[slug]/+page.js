export async function load({ params }){
  const post = await import(`../posts/${params.slug}.md`)
  const { title, description } = post.metadata
  const content = post.default

  return {
    content,
    description,
    title,
  }
}
