// Code from
// https://joshcollinsworth.com/blog/build-static-sveltekit-markdown-blog
import { fetchMarkdownNotes } from '$lib/utils'
import { json } from '@sveltejs/kit'

export const GET = async () => {
  const allPosts = await fetchMarkdownNotes()
  return json(allPosts)
}
