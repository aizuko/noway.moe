// Code from
// https://joshcollinsworth.com/blog/build-static-sveltekit-markdown-blog
import { fetchDuckieTownPosts } from '$lib/utils'
import { json } from '@sveltejs/kit'

export const GET = async () => {
  const allPosts = await fetchDuckieTownPosts()
  const sortedPosts = allPosts
    .sort((a, b) => b.meta.entry_number - a.meta.entry_number)

  return json(sortedPosts)
}
