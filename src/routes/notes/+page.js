export const load = async ({ fetch }) => {
    const response = await fetch("/api/notes")
    const notes = await response.json()
    console.log(notes)
    return { notes }
}
