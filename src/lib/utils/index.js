// Code from
// https://joshcollinsworth.com/blog/build-static-sveltekit-markdown-blog
export const fetchMarkdownNotes = async () => {
  const allPostFiles = import.meta.glob('/src/routes/notes/*.md')
  const iterablePostFiles = Object.entries(allPostFiles)

  const allPosts = await Promise.all(
    iterablePostFiles.map(async ([path, resolver]) => {
      const { metadata } = await resolver()
      const postPath = path.slice(11, -3)

      return {
        meta: metadata,
        path: postPath,
      }
    })
  )

  return allPosts
}

export const fetchDuckieTownPosts = async () => {
  const allPostFiles = import.meta.glob('/src/routes/duckietown/posts/*.md')
  const iterablePostFiles = Object.entries(allPostFiles)

  const allPosts = await Promise.all(
    iterablePostFiles.map(async ([path, resolver]) => {
      const { metadata } = await resolver()
      const postPath = "duckietown/" + path.slice(29, -3)

      return {
        meta: metadata,
        path: postPath,
      }
    })
  )

  return allPosts
}
