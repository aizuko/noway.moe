import adapter from '@sveltejs/adapter-cloudflare';
import preprocess from 'svelte-preprocess';
import { mdsvex } from 'mdsvex';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	//preprocess: preprocess(),

	extensions: ['.svelte', '.md'],

	kit: {
		adapter: adapter()
	},

	preprocess: [
		preprocess(),
		mdsvex({
			extensions: ['.md'],
		})
	]
};

export default config;
