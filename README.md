# My Webpage

Personal website written in SvelteKit. You can visit it live
[here!](https://noway.moe). The layout follow SvelteKit guidelines

## Building

I was told to use `yarn`, so here are the steps for that

```bash
please pacman -S yarn
git clone 'https://codeberg.org/akemi/noway.moe' site
cd site
yarn install
yarn dev
yarn build
yarn preview
```

Use `deploy.sh` for Cloudflare Pages
