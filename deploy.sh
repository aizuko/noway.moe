#!/usr/bin/env bash
yarn build || exit $?
export CLOUDFLARE_ACCOUNT_ID="$(pass show cloudflare/account_id)"
npx wrangler pages publish .svelte-kit/cloudflare
